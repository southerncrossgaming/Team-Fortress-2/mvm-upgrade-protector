#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <tf2_stocks>

bool b_IsMVMActive = false;

#define		STR_AUTO_CHAT	"\x01[\x07A6FF00MVM\x01]"

public Plugin:myinfo =
{
	name = "[TF2] Canteen saver",
	author = "Rowedahelicon",
	description = "Stops players from wasting Canteens!",
	version = "1.0.0",
	url = "https://www.rowedahelicon.com"
}

public onMapStart(){
    b_IsMVMActive = false;
}

public OnPluginStart()
{
    new mvmLogic = FindEntityByClassname(-1, "tf_logic_mann_vs_machine");
	if (mvmLogic == -1)
	{
        SetFailState("This plugin only works on an MVM map.");
    }
    
    HookEvent("mvm_begin_wave", Event_RoundStart, EventHookMode_Pre);
    HookEvent("mvm_wave_complete", Event_RoundEnd, EventHookMode_Pre);
    HookEvent("mvm_mission_complete", Event_RoundEnd, EventHookMode_Pre);
    HookEvent("mvm_wave_failed", Event_RoundEnd, EventHookMode_Pre);
    HookEvent("mvm_mission_update", Event_RoundEnd, EventHookMode_Pre);
}

public Action OnClientCommandKeyValues(int client, KeyValues kv)
{
    if(b_IsMVMActive == false && IsPlayerAlive(client)){
        char command[64];
        kv.GetSectionName(command, sizeof(command));

        if (StrEqual(command, "+use_action_slot_item_server")){

			int canteen = FindUserCanteen(client);
			if(canteen != -1)
			{
				if(GetEntProp(canteen, Prop_Send, "m_usNumCharges") > 0){
					ForcePlayerSuicide(client);
                    PrintToChat(client, "%s You were killed to avoid accidentally spending your canteen before round start!", STR_AUTO_CHAT);
                }
			}
        }
    }
}

public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
    b_IsMVMActive = true;
}

public Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
    b_IsMVMActive = false;
}

//Credit for this goes to Quake84
//https://forums.alliedmods.net/showthread.php?p=2431845
stock int FindUserCanteen(int client)
{
	if(IsPlayerAlive(client))
	{
		int i = -1;
		while ((i = FindEntityByClassname(i, "tf_powerup_bottle")) != -1)
		{
			if (IsValidEntity(i) && GetEntPropEnt(i, Prop_Send, "m_hOwnerEntity") == client && !GetEntProp(i, Prop_Send, "m_bDisguiseWearable"))
			{
				return i;
			}
		}
		return i;
	} else
		return -1;
}